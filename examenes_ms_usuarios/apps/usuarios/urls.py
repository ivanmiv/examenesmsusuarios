from django.urls import path
from .views import *

urlpatterns = [
    path('auth', AutenticarUsuario.as_view(), name='usuarios_autenticar'),
    path('', ListarCrearUsuarioViewSet.as_view()),
    path('<int:pk>/', ObtenerModificarUsuarioViewSet.as_view(), name='usuario-detail'),
]
